
document.addEventListener("deviceready", onDeviceReady, false);

    function onDeviceReady() {
        $( "#popupLoginWindow" ).popup( "open" );
        var db = window.sqlitePlugin.openDatabase({name: "db_app.db"});
        show_list_local();
        $('#popupEdit').css('overflow-y', 'scroll');
        $('.ui-field-contain').css('overflow-y', 'scroll');
        $('#edit_describe').css('overflow-y', 'scroll');
        $("textarea#add_describe").css({'height':'100px', 'width':'250px'});

        function show_list_local(){

             db.transaction(function(transaction) {

                transaction.executeSql('SELECT id_lokal, nazwa FROM LOKAL', [], function (tx, results) {
                 var pair="<thead><tr><th data-priority='1'><center>Id</center></th><th data-priority='2'><center>Nazwa</center></th><th data-priority='3'><center>Edytuj</center></th><th data-priority='4'><center>Usuń</center></th></tr></thead>";
                 var i=0;
                 var len = results.rows.length, i;
                 for (i=0; i<=len-1; i++) {
                   var local_name = results.rows.item(i).nazwa;
                   var id_local = results.rows.item(i).id_lokal;
                   pair += "<tr><td id='ui-center'>"+id_local+"</td><td id='ui-center'>"+local_name+"</td><td><center><a href='#popupEdit' data-rel='popup' data-custom="+id_local+" id='update_local' class='ui-btn ui-shadow ui-corner-all ui-icon-edit ui-btn-icon-notext'></a></center></td><td><a data-custom="+id_local+" id='delete_local'  class='ui-btn ui-shadow ui-corner-all ui-icon-delete ui-btn-icon-notext'></a></td></tr>";
                 }
                 if (pair == "<tr><th>Nazwa</th></tr>") {
                   pair += "<tr><td><i>empty</i></td></tr>";
                 }
                 $("#table_local_list").html(pair);

                 }, null);
             });
        }

        $(document).on('click', '#category_panel', function(){
               value_type = document.getElementById('category').value
                 show();
                 $mobile.changePage('#pageone')
                });

        $(document).on('click', '#update_local', function(){
             id =  $(this).attr('data-custom');
             $("#id_local_edit").val(id);
             db.transaction(function(transaction) {
                transaction.executeSql('SELECT nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email FROM LOKAL where id_lokal=?', [id], function (tx, results) {
                   var name_edit_local = results.rows.item(0).nazwa;
                   var desc_edit_local = results.rows.item(0).opis;
                   var street_edit_local = results.rows.item(0).ulica;
                   var city_edit_local = results.rows.item(0).miasto;
                   var postal_edit_local = results.rows.item(0).kod_pocztowy;
                   var tel_edit_local = results.rows.item(0).telefon;
                   var email_edit_local = results.rows.item(0).email;
                    $("#edit_name").val(name_edit_local);
                    $("#edit_describe").val(desc_edit_local);
                    $("#edit_street").val(street_edit_local);
                    $("#edit_city").val(city_edit_local);
                    $("#edit_postal").val(postal_edit_local);
                    $("#edit_tel").val(tel_edit_local);
                    $("#edit_email").val(email_edit_local);

                 },
                 function(error){
                     alert('Something went Wrong');
                 });
             });
        });


        $(document).on('click', '#btn_upload', function(){

            var get_name_edit_local = document.getElementById("edit_name").value;
            var get_desc_edit_local = document.getElementById("edit_describe").value;
            var get_street_edit_local = document.getElementById("edit_street").value;
            var get_city_edit_local = document.getElementById("edit_city").value;
            var get_postal_edit_local = document.getElementById("edit_postal").value;
            var get_tel_edit_local = document.getElementById("edit_tel").value;
            var get_email_edit_local = document.getElementById("edit_email").value;

                db.transaction(function(transaction) {
                     transaction.executeSql('UPDATE LOKAL SET nazwa = ?, opis = ?, ulica = ?, miasto = ?, kod_pocztowy = ?, telefon = ?, email = ? where id_lokal=? ', [get_name_edit_local, get_desc_edit_local, get_street_edit_local, get_city_edit_local, get_postal_edit_local, get_tel_edit_local, get_email_edit_local, id], function (tx, results){
                     show_list_local();
                    alert('Informacje zostały edytowane!');
                    },
                     function(error){
                        alert('Spróbuj jeszcze raz');
                      });


             });
         });

         $(document).on('click', '#delete_local', function(){
            id_delete_local =  $(this).attr('data-custom');
              db.transaction(function(transaction) {
                   transaction.executeSql('DELETE TABLE LOKAL where id_lokal=? ', [id_delete_local], function (tx, results){
                     show_list_local();
                        alert('Lokal został usunięty!');
                        },
                       function(error){
                       alert('Spróbuj jeszcze raz');
                       });


                   });


         });

        $(document).on('click', '#login', function(){

            var loginAdmin = document.getElementById('login_admin').value;
            var passwordAdmin = document.getElementById('password_admin').value;

            if(loginAdmin == 'admin' && passwordAdmin == 'admin'){
                $( "#popupLoginWindow" ).popup( "close" );
            }else{
                alert('Nieprawidłowy login lub hasło');
            }
         });

        $(document).on('click', '#btn_add_local', function(){

            var get_name_add_local = document.getElementById("add_name").value;
            var get_desc_add_local = document.getElementById("add_describe").value;
            var get_tel_add_local = document.getElementById("add_tel").value;
            var get_email_add_local = document.getElementById("add_email").value;
            var get_street_add_local = document.getElementById("add_street").value;
            var get_postal_add_local = document.getElementById("add_postal").value;
            var get_city_add_local = document.getElementById("add_city").value;
            var get_lng_add_local = document.getElementById("add_lng").value;
            var get_lat_add_local = document.getElementById("add_lat").value;
            var get_category_add_local = document.getElementById("select_add_category").value;

            db.transaction(function(transaction) {
            transaction.executeSql('INSERT INTO LOKAL (nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email, miniatura, ocena, id_typ) VALUES (get_name_add_local, get_desc_add_local,get_street_add_local,get_city_add_local,get_postal_add_local,get_tel_add_local,get_email_add_local,path_photo,0,select_add_category) ', [get_name_edit_local, get_desc_edit_local, get_street_edit_local, get_city_edit_local, get_postal_edit_local, get_tel_edit_local, get_email_edit_local, id], function (tx, results){
            show_list_local();
            alert('Lokal został dodany!');
        },
         function(error){
            alert('Spróbuj jeszcze raz');
             });
         });
    });
}