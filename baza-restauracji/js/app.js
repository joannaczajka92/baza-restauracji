document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {

  var db = window.sqlitePlugin.openDatabase({name: "db_app.db"});

  db.transaction(function(tx) {
   /* tx.executeSql('INSERT INTO LOKALIZACJA (dlugosc_geo, szerokosc_geo, id_lokal) VALUES (50.0296, 22.0156, 5)');
      tx.executeSql('INSERT INTO LOKALIZACJA (dlugosc_geo, szerokosc_geo, id_lokal) VALUES (50.03629, 22.00109, 6)');
      tx.executeSql('INSERT INTO LOKAL (nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email, miniatura, ocena, id_typ) VALUES ("Chilita - pizza e pasta","Jest takie miejsce w Rzeszowie gdzie w otoczeniu starych tynków, świeżych ziół, dębowych stołów i zapachu drzewa palonego w piecu, odnaleźć można klimat wakacji, może właśnie tych spędzonych ostatnio.","Rejtana 23","Rzeszów","35-326",178654414,"chilita@chilita.pl","image/miniatura/5_min.png",3,1)');
      tx.executeSql('INSERT INTO LOKAL (nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email, miniatura, ocena, id_typ) VALUES ("Tartuffo","Restauracja Tartuffo to jedno z najpiękniejszych i najciekawszych punktów na kulinarnej mapie Rzeszowa. Położona przy tętniącej życiem ulicy Słowackiego w sąsiedztwie zabytkowych ulic starówki Rzeszowa. Łączy w sobie niepowtarzalny modernistyczny design, doskonałą kuchnie i dobre brzmienie muzyki na żywo.","Słowackiego 16","Rzeszów","35-060",530988199,"restauracja.tartuffo@gmail.com","image/miniatura/5_min.png",3,1)');
      tx.executeSql('CREATE TABLE IF NOT EXISTS TYP (id_typ integer primary key, nazwa varchar(100))');
      tx.executeSql('INSERT INTO TYP (nazwa) VALUES ("restauracja")');
      tx.executeSql('INSERT INTO TYP (nazwa) VALUES ("pizzeria")');
      tx.executeSql('INSERT INTO TYP (nazwa) VALUES ("fast food")');
      tx.executeSql('INSERT INTO TYP (nazwa) VALUES ("cukiernia")');
      tx.executeSql('CREATE TABLE IF NOT EXISTS LOKAL (id_lokal integer primary key, nazwa varchar(150), opis text, ulica varchar(150), miasto varchar(50), kod_pocztowy varchar(50), telefon integer, email varchar(100), miniatura text, ocena integer, id_typ INTEGER, FOREIGN KEY(id_typ) REFERENCES TYP (id_typ))');
      tx.executeSql('INSERT INTO LOKAL (nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email, miniatura, ocena, id_typ) VALUES ("Stary Browar Rzeszowski","Pierwszy w Rzeszowie minibrowar STARY BROWAR RZESZOWSKI zlokalizowany jest bezpośrednio przy Rynku Starego Miasta. Został on zaprojektowany tak, aby móc zaspokoić nawet najwybredniejsze podniebienia. Serwujemy tu 4 gatunki piwa własnej produkcji","Rynek 20/23","Rzeszów","35-064",172500015,"sbr@stary-browar.pl","image/miniatura/1_min.png",5,1)');
      tx.executeSql('INSERT INTO LOKAL (nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email, miniatura, ocena, id_typ) VALUES ("Kuk Nuk","KUK NUK to nowoczesna, autorska kuchnia Macieja Stankiewicza, przygotowywana wyłącznie w oparciu o świeże, regionalne produkty dostarczane codziennie przez producentów z Podkarpacia. Sezonowa karta, zmieniająca się wraz z porami roku to najlepsza wizytówka i znak firmowy KUK NUK.","Jana Matejki 4","Rzeszów","35-069",533219000,"kuknuk@kuknuk.pl","image/miniatura/2_min.png",5,1)');
      tx.executeSql('INSERT INTO LOKAL (nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email, miniatura, ocena, id_typ) VALUES ("Hola Lola","Z nami można zacząć dzień od śniadania, aromatycznej kawy i prasy. Można wpaść w biegu na szybką przekąskę, świeża sałatkę w środku dnia pracy, uczelni czy szkoły. W Hola gwarantujemy atmosferę sprzyjającą też spokojnemu delektowaniu się zapachem i smakiem świeżo palonej kawy.","Mickiewicza 3","Rzeszów","35-064",730119180, "holalola@holalola.pl" ,"image/miniatura/3_min.png",4,1)');
      tx.executeSql('INSERT INTO LOKAL (nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email, miniatura, ocena, id_typ) VALUES ("Niebieskie Migdały","Pomyśl o miejscu, stworzonym z miłości do jedzenia, z tęsknoty za ciepłem i zapachem rodzinnego domu, z marzeń o sjeście pod błękitem słonecznej Italii… O miejscu gdzie szacunek dla polskiej tradycji i miłość do włoskiej kuchni tworzą sentymentalny duet, miejsce, skąd możesz wyruszyć w podróż do krainy łagodności o smaku i zapachu czterech pór roku.","3 Maja 8","Rzeszów","35-030",177844128,"kontakt@niebieskie-migdaly.pl","image/miniatura/4_min.png",4,1)');
      tx.executeSql('CREATE TABLE IF NOT EXISTS OPINIA (id_opinia integer primary key, tresc text, podpis varchar(150), ocena integer, id_lokal INTEGER, FOREIGN KEY(id_lokal) REFERENCES LOKAL (id_lokal) )');
      tx.executeSql('INSERT INTO OPINIA (tresc, podpis, ocena, id_lokal) VALUES ("Pyszne jedzenie i miła obsługa, polecam!", "Joanna",5, 1)');
      tx.executeSql('INSERT INTO OPINIA (tresc, podpis, ocena, id_lokal) VALUES ("Restauracja na wysokim poziomie.", "User122",5, 1)');
      tx.executeSql('INSERT INTO OPINIA (tresc, podpis, ocena, id_lokal) VALUES ("Lokal godny polecenia!", "Alex",5, 2)');
      tx.executeSql('CREATE TABLE IF NOT EXISTS ZDJECIE (id_zdjecie integer primary key, adres text, id_lokal INTEGER, FOREIGN KEY(id_lokal) REFERENCES LOKAL (id_lokal) )');
      tx.executeSql('INSERT INTO ZDJECIE (adres, id_lokal) VALUES ("image/stary_browar/browar_01.jpg", 1)');
      tx.executeSql('INSERT INTO ZDJECIE (adres, id_lokal) VALUES ("image/stary_browar/browar_02.jpg", 1)');
      tx.executeSql('INSERT INTO ZDJECIE (adres, id_lokal) VALUES ("image/stary_browar/browar_03.jpg", 1)');
      tx.executeSql('INSERT INTO ZDJECIE (adres, id_lokal) VALUES ("image/stary_browar/browar_04.jpg", 1)');
      tx.executeSql('INSERT INTO ZDJECIE (adres, id_lokal) VALUES ("image/kuknuk/kuknuk_01.jpg", 2)');
      tx.executeSql('INSERT INTO ZDJECIE (adres, id_lokal) VALUES ("image/kuknuk/kuknuk_02.jpg", 2)');
      tx.executeSql('INSERT INTO ZDJECIE (adres, id_lokal) VALUES ("image/kuknuk/kuknuk_03.jpg", 2)');
      tx.executeSql('CREATE TABLE IF NOT EXISTS LOKALIZACJA (id_lokalizacja integer primary key, dlugosc_geo real, szerokosc_geo real, id_lokal INTEGER, FOREIGN KEY(id_lokal) REFERENCES LOKAL (id_lokal) )');
      tx.executeSql('INSERT INTO LOKALIZACJA (dlugosc_geo, szerokosc_geo, id_lokal) VALUES (50.0377, 22.0050, 1)');
      tx.executeSql('INSERT INTO LOKALIZACJA (dlugosc_geo, szerokosc_geo, id_lokal) VALUES (50.0379, 22.0038, 2)');
      tx.executeSql('INSERT INTO LOKALIZACJA (dlugosc_geo, szerokosc_geo, id_lokal) VALUES (50.0376, 22.0036, 3)');
  	  tx.executeSql('INSERT INTO LOKALIZACJA (dlugosc_geo, szerokosc_geo, id_lokal) VALUES (50.0372, 21.9992, 4)');

   */

   $(document).on('click', '#category', function(){
          value_type = document.getElementById('category').value
            show();
            $mobile.changePage('#pageone')
    });


$(document).on('click', '#category_panel', function(){
       value_type = document.getElementById('category').value
         show();
         $mobile.changePage('#pageone')
        });

     });
    function show(){
         db.transaction(function(transaction) {
         transaction.executeSql('SELECT id_lokal, nazwa, opis, miniatura, ocena FROM LOKAL WHERE id_typ = ?', [value_type], function (tx, results) {

            var i=0;
            var j=0;
            var len = results.rows.length, i;
            for (i=0; i<=len-1; i++) {
                var id_local=results.rows.item(i).id_lokal;
                var name_local = results.rows.item(i).nazwa;
                var describe_local = results.rows.item(i).opis;
                var min_local=results.rows.item(i).miniatura;
                var count_star=results.rows.item(i).ocena;
                $('#element_listview').append(' <div data-role="collapsible" id="col" data-filter="true" data-input="#autocomplete-input" data-theme="a">' +
                '<h4><div class="hotel_content"> <div class="img"><img src='+min_local+'></div><div class="name_hotel">'+name_local+
                '<div class="rating" id="rating_local"><div id="add_star_'+id_local+'">'+
                '</div></div></div></div></h4>'+
                '<ul data-role="listview" class="more_list" >'+
                '<li><div id="describe">'+describe_local+'</div><a href="#" class="more" id='+id_local+'>Więcej</a></li>'+
                '</ul></div>' );
                    for (j=0; j<=count_star-1; j++) {
                    $('#add_star_'+id_local+'').append('<a href="#" data-role="button" data-inline="true" data-icon="star" data-vote="1" data-iconpos="notext" data-theme="e"></a>');
                    }
                }
             }, null);
        });

    }

    var passDataObject = { selectedId: null }

    $(document).on( "pageinit", "#pageone", function( e ) {
       $("ul.more_list").find('a').unbind('click').click(function(e) {
          e.preventDefault();
          passDataObject.selectedId = this.id;
          $.mobile.changePage('#pagetwo', { transition: 'slide'} );
        });
    });


    $(document).on( "pagebeforeshow", "#pagetwo", function( e ) {
        id_local_list=passDataObject.selectedId;
        show_info_local();
        get_lng_lat();

        $('#location_tap').on("tap",function(){
        initialize_map();
        });

        $(window).resize(function(){
         initialize_map();
         });

    });

    function show_info_local(){

        db.transaction(function(transaction) {
            transaction.executeSql('SELECT nazwa, opis, ulica, miasto, kod_pocztowy, telefon, email, ocena  FROM LOKAL WHERE id_lokal=?', [id_local_list], function (tx, results) {
            var j=0;
            var name_local_info=results.rows.item(0).nazwa;
            var describe_info=results.rows.item(0).opis;
            var street_info=results.rows.item(0).ulica;
            var city_info=results.rows.item(0).miasto;
            var postal_code_info=results.rows.item(0).kod_pocztowy;
            var telefon_info=results.rows.item(0).telefon;
            var email_info=results.rows.item(0).email;
            var rating_add_info=results.rows.item(0).ocena;
             $('#star_rating_info').empty();

            $('#name_local_info').html(name_local_info);
            $('#describe_info').html('<p>'+describe_info+'</p>');
            $('#street_info').html(street_info);
            $('#postal_code_info').html(postal_code_info+' '+city_info);
            $('#telefon_info').html('Tel. '+telefon_info);
            $('#email_info').html('e-mail: '+email_info);
            //$('#star_rating_info').empty();
            for (j=0; j<=rating_add_info-1; j++) {
            $('#star_rating_info').append('<a href="#" data-role="button" data-inline="true" data-icon="star" data-vote="1" data-iconpos="notext" data-theme="e"></a>').enhanceWithin();
            }
        }, null);

        transaction.executeSql('SELECT tresc, podpis, id_lokal FROM OPINIA WHERE id_lokal=?', [id_local_list], function (tx, results) {
            $('#user_comment_div').empty();
            var j=0;
            var len = results.rows.length, j;
            for (j=0; j<=len-1; j++) {
                var text_comment=results.rows.item(j).tresc;
                var signature_comment=results.rows.item(j).podpis;
                $('#user_comment_div').append('<div id="user_comment">'+signature_comment+':  '+text_comment+'</div>');
            }

         }, null);
        });
         show_photo_local();
    }

    function show_photo_local(){
        db.transaction(function(transaction) {
            $('#gallery').empty();
            $('#popup_img_open').empty();
            transaction.executeSql('SELECT id_zdjecie, adres, id_lokal FROM ZDJECIE WHERE id_lokal=?', [id_local_list], function (tx, results) {
                $('#gallery').empty();
                var j=0;
                var len = results.rows.length, j;

                for (j=0; j<=len-1; j++) {
                    var address_photo=results.rows.item(j).adres;
                    var id_photo=results.rows.item(j).id_zdjecie;
                    $('#gallery').append('<a href="#popup'+id_photo+'" data-rel="popup" data-position-to="window" data-transition="fade">'+
                    '<img  src='+address_photo+' id="popimg"></a>');
                }
                j=0;
                for (j=0; j<=len-1; j++) {
                    var address_photo_popup=results.rows.item(j).adres;
                    var id_photo_popup=results.rows.item(j).id_zdjecie;
                    $('#popup_img_open').append('<div data-role="popup" id="popup'+id_photo_popup+'"  data-overlay-theme="a">'+
                    '<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close'+
                    '</a><img src='+address_photo_popup+' style="max-height:512px;" ></div>').enhanceWithin();
                }

            }, null);
         });
    }


    function get_lng_lat(){
       db.transaction(function(transaction) {
            transaction.executeSql('SELECT dlugosc_geo, szerokosc_geo, id_lokal FROM LOKALIZACJA WHERE id_lokal=?', [id_local_list], function (tx, results) {
                longitude_local=results.rows.item(0).dlugosc_geo;
                latitude_local=results.rows.item(0).szerokosc_geo;
                longitude=parseFloat(longitude_local);
                latitude=parseFloat(latitude_local);
             }, null);

        });
    }

    function initialize_map(){

        var myLatLng = new google.maps.LatLng(longitude, latitude)
                        var myOptions = {
                        center: myLatLng,
                        zoom: 17,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    var map = new google.maps.Map(document.getElementById("map"), myOptions);

                    var marker = new google.maps.Marker({
                    position: myLatLng,
                    title:"Tutaj jesteśmy!"
                    });
                    marker.setMap(map);

        google.maps.event.addListenerOnce(map, 'idle', function() {
        google.maps.event.trigger(map, 'resize');
        map.setCenter(myLatLng); // var center = new google.maps.LatLng(50,3,10.9);
        });

        $('#map').css("height",$(window).height());
        $('#map').css("width",$(window).width());

        map.setZoom( map.getZoom() );

    }


    $(document).on('click', '#add_comment', function(){

        var signature_add= $('#signature').val();
        var text_comment_add=$('#text_comment').val();
        var select_rating_add=$('#select_rating').val();
        db.transaction(function(transaction) {
          transaction.executeSql('INSERT INTO OPINIA (tresc, podpis, ocena, id_lokal) VALUES (?,?,?,?)', [text_comment_add,signature_add,select_rating_add,id_local_list], function(tx, result) {
            show_info_local();
            $('#popupComment').popup('close');
          },
            function(error){
            alert('Problem z bazą danych');
            });
        });
    });

    $(document).on('click', '#login_window', function(){
    $( "#popupLoginWindow" ).popup( "open" );
    });

}






